# Terraform to make a GCP environment

How to use:

Fetch the google provider:

```sh
$ terraform init
```

Then you can run with the following:

```sh
TF_VAR_ssh_user=$(whoami) TF_VAR_ssh_public_key=$(cat ~/.ssh/id_rsa.pub) TF_VAR_prefix=rclamp GCLOUD_ZONE=europe-west4-a GCLOUD_KEYFILE_JSON=testground-93f3eca8802c.json GCLOUD_PROJECT=testground-184911 terraform plan
```

`GCLOUD_KEYFILE_JSON` is an authentication file https://www.terraform.io/docs/providers/google/index.html#authentication-json-file

