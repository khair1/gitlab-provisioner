# CI configuration

## Variables

These should be supplied as secret variables

|name|description|
|-|-|
|`TF_VAR_ssh_user`|username to use for SSH|
|`SSH_PRIVATE_KEY`|key to use when controlling nodes|
|`SSH_PUBLIC_KEY`|key to use when provisioning nodes|
|`GCLOUD_CREDENTIALS_JSON`|json of credentials to make instances with|
|`GCLOUD_ZONE`|gcloud availability zone to make the instances in|
|`STATE_GCS_BUCKET`|name of bucket for terraform state|
|`postgresql_gitlab_consul_user_password`|`postgresql_gitlab_consul_user_password` in ansible|
|`postgresql_pgbouncer_user_password`|`postgresql_pgbouncer_user_password` in ansible|
|`postgresql_sql_user_password`|`postgresql_sql_user_password` in ansible|
